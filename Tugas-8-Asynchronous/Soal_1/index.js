// Soal No. 1 (Callback Baca Buku)

var readBooks = require('./callback.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

let x = 0;
let waktu = 10000

function waktu_panggil() {
    readBooks(t, books[x], function (sisa_waktu) {
        t = sisa_waktu
        x++
        if (x < books.length)
            waktu_panggil()
    })
}
waktu_panggil();
