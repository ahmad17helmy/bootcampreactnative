// Soal No. 2 (Promise Baca Buku)

var readBooksPromise = require('./promise.js')

var books = [
    { name: 'LOTR', timeSpent: 3000 },
    { name: 'Fidas', timeSpent: 2000 },
    { name: 'Kalkulus', timeSpent: 4000 }
]

async function waktu_panggil() {
    let waktu = 10000
    for (let x = 0; x < books.length; x++) {
        waktu = await readBooksPromise(waktu, books[x])
            .then(function (sisa_waktu) {
                return sisa_waktu;
            })
            .catch(function (sisa_waktu) {
                return sisa_waktu;
            });
    }o
    console.log("selesai")
}
waktu_panggil();
