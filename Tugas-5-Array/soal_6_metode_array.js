// Soal No. 6 (Metode Array)

var input = ["0001", "Roman Alamsyah ", "Bandar Lampung", "21/05/1989", "Membaca"];
dataHandling2(input);

function dataHandling2(result) {
  var data_input = result

  var nama = result[1] + 'Elsharawy' 
  var provinsi = 'Provinsi ' + result[2]
  var jenis_kelamin = 'Pria'
  var sekolah = 'SMA Internasional Metro'

  data_input.splice(1, 1, nama)
  data_input.splice(2, 1, provinsi)
  data_input.splice(4, 1, jenis_kelamin, sekolah)

  var tanggal = result[3]
  var tanggal_tukar = tanggal.split('/') 
  var bulan = tanggal_tukar[1]
  var nama_bulan = ''

  switch (bulan) {
    case '01':
      nama_bulan = 'Januari'; break;
    case '02':
      nama_bulan = 'Februari'; break;
    case '03':
      nama_bulan = 'Maret'; break;
    case '04':
      nama_bulan = 'April'; break;
    case '05':
      nama_bulan = 'Mei'; break;
    case '06':
      nama_bulan = 'Juni'; break;
    case '07':
      nama_bulan = 'Juli'; break;
    case '08':
      nama_bulan = 'Agustus'; break;
    case '09':
      nama_bulan = 'September'; break;
    case '10':
      nama_bulan = 'Oktober'; break;
    case '11':
      nama_bulan = 'November'; break;
    case '12':
      nama_bulan = 'Desember'; break;
    default: break;
  }

  var nama_ubah = nama.slice(0, 15)
  var tanggal_join = tanggal_tukar.join('-')
  var tanggal_hasil = tanggal_tukar.sort(function (hasil1, hasil2) 
  { 
    return hasil2 - hasil1 
  })

  console.log(data_input) 
  console.log(nama_bulan)
  console.log(tanggal_hasil) 
  console.log(tanggal_join) 
  console.log(nama_ubah) 
}