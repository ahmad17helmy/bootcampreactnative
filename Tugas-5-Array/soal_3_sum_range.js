// Soal No. 3 (Sum of Range)

function sum(startNum, finishNum, step) {
  var numValue = [];
  var numRange;

  if (!step) {
    numRange = 1
  } else {
    numRange = step
  }

  if (startNum > finishNum) {
    var number = startNum;
    for (var i = 0; number >= finishNum; i++) {
      numValue.push(number)
      number -= numRange
    }
  } else if (startNum < finishNum) {
    var number = startNum;
    for (var i = 0; number <= finishNum; i++) {
      numValue.push(number)
      number += numRange
    }
  } else if (!startNum && !finishNum && !step) {
    return 0
  } else if (startNum) {
    return startNum
  }

  var total = 0;
  for (var i = 0; i < numValue.length; i++) {
    total = total + numValue[i]
  }
  return total
}

console.log(sum(1, 10)) 
console.log(sum(5, 50, 2))
console.log(sum(15, 10)) 
console.log(sum(20, 10, 2))
console.log(sum(1)) 
console.log(sum()) 