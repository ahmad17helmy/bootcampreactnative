// Soal No. 4 (Array Multidimensi)

var input = [
  ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"], // 0
  ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"], // 1
  ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"], // 2
  ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"] // 3
];

function dataHandling(result) {
  var inputLength = result.length
  for (var i = 0; i < inputLength; i++) {
    var id = 'Nomor ID: ' + result[i][0]
    var nama = 'Nama Lengkap: ' + result[i][1]
    var ttlahir = 'TTL: ' + result[i][2] + ' ' + result[i][3]
    var hobi = 'Hobi: ' + result[i][4]
    console.log(id)
    console.log(nama)
    console.log(ttlahir)
    console.log(hobi)
    console.log()
  }
}

dataHandling(input);