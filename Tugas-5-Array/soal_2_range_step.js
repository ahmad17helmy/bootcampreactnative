// Soal No. 2 (Range with Step)

function rangeWithStep(startNum, finishNum, step) {
  var numValue = [];

  if (startNum > finishNum) {
    var num = startNum;
    for (var i = 0; num >= finishNum; i++) {
      numValue.push(num)
      num -= step
    }
  } else if (startNum < finishNum) {
    var num = startNum;
    for (var i = 0; num <= finishNum; i++) { 
      numValue.push(num)
      num += step
    }
  } else if (!startNum || !finishNum || !step) {
    return -1
  }
  return numValue
}

console.log(rangeWithStep(1, 10, 2)) 
console.log(rangeWithStep(11, 23, 3)) 
console.log(rangeWithStep(5, 2, 1)) 
console.log(rangeWithStep(29, 2, 4)) 