//Soal No. 3 Mengurai Kalimat (Substring)

var sentence3 = 'wow JavaScript is so cool'; 

var exampleFirstWord3 = sentence3.substring(0, 3); 
var secondWord3 = sentence3.substring(4, 14);
var thirdWord3 = sentence3.substring(15, 17);
var fourthWord3= sentence3.substring(18, 20);
var fifthWord3 = sentence3.substring(21, 25);

console.log('First Word: ' + exampleFirstWord3); 
console.log('Second Word: ' + secondWord3); 
console.log('Third Word: ' + thirdWord3); 
console.log('Fourth Word: ' + fourthWord3); 
console.log('Fifth Word: ' + fifthWord3);