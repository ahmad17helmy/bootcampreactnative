// Soal No. 4 Mengurai Kalimat dan Menentukan Panjang String

var sentence2 = 'wow JavaScript is so cool'; 

var exampleFirstWord2 = sentence2.substring(0, 3); 
var secondWord2 = sentence2.substring(4, 14);
var thirdWord2 = sentence2.substring(15, 17);
var fourthWord2= sentence2.substring(18, 20);
var fifthWord2 = sentence2.substring(21, 25);

var firstWordLength = exampleFirstWord2.length  
var secondWordLength = secondWord2.length
var thirdWordLength = thirdWord2.length
var fourthWordLength = fourthWord2.length
var fifthWordLength = fifthWord2.length

console.log('First Word: ' + exampleFirstWord2 + ', with length: ' + firstWordLength); 
console.log('Second Word: ' + secondWord2 + ', with length: ' + secondWordLength); 
console.log('Third Word: ' + thirdWord2 + ', with length: ' + thirdWordLength); 
console.log('Fourth Word: ' + fourthWord2 + ', with length: ' + fourthWordLength); 
console.log('Fifth Word: ' + fifthWord2 + ', with length: ' + fifthWordLength);