// Switch Case

// Soal
// Kamu akan diberikan sebuah tanggal dalam tiga variabel, yaitu hari, bulan, dan tahun. Disini kamu diminta untuk membuat format tanggal. 
// Misal tanggal yang diberikan adalah hari 1, bulan 5, dan tahun 1945. Maka, output yang harus kamu proses adalah menjadi 1 Mei 1945.

var tanggal = 1; 
var bulan = 5; 
var tahun = 1945; 
var nama_bulan;

switch (true) {
  case (tanggal < 1 || tanggal > 31): {
    console.log('Tanggal salah')
    break;
  }
   case (bulan < 1 || bulan > 12):
    console.log('Bulan Salah')
    break;
  case (tahun < 1900 || tahun > 2200): {
    console.log('Tahun Salah')
    break;
  }
  default:
    {
      switch (true) {
        case bulan == 1:
          nama_bulan = 'Januari';
          break;
        case bulan == 2:
          nama_bulan = 'Februari';
          break;
        case bulan == 3:
          nama_bulan = 'Maret';
          break;
        case bulan == 4:
          nama_bulan = 'April';
          break;
        case bulan == 5:
          nama_bulan = 'Mei';
          break;
        case bulan == 6:
          nama_bulan = 'Juni';
          break;
        case bulan == 7:
          nama_bulan = 'Juli';
          break;
        case bulan == 8:
          nama_bulan = 'Agustus';
          break;
        case bulan == 9:
          nama_bulan = 'September';
          break;
        case bulan == 10:
          nama_bulan = 'Oktober';
          break;
        case bulan == 11:
          nama_bulan = 'November';
          break;
        case bulan == 12:
          nama_bulan = 'Desember';
          break;
        default:
          break;
      }
      console.log(tanggal, ' ', nama_bulan, ' ', tahun)
      break;
    }
}