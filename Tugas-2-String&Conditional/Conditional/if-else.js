// If-else

// Output untuk Input nama = '' dan peran = ''
// "Nama harus diisi!"

var nama = ""
var peran = ""
 
if ( nama == "" && peran=="") {
    console.log("Nama harus diisi!")
    }

//Output untuk Input nama = 'John' dan peran = ''
//"Halo John, Pilih peranmu untuk memulai game!"

var nama2 = "John"
var peran2 = ""

if (peran2 == "") {
    console.log("Halo " + nama2 + ", Pilih peranmu untuk memulai game!")
    }

//Output untuk Input nama = 'Jane' dan peran 'Penyihir'
//"Selamat datang di Dunia Werewolf, Jane"
//"Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!"

var nama3 = "Jane"
var peran3 = "Penyihir"

if (nama3 == '') {
  console.log('Nama harus diisi!')
} else if (nama3 && peran3 == '') {
  console.log('Halo ' + nama3 + ', Pilih Peranmu untuk memulai game')
} else if (nama3 == 'Jane' && peran3 == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, " + nama3 + "\nHalo " +peran3 + " " +  nama3 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama3 == 'Jenita' && peran3 == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, " + nama3 + "\nHalo " +peran3 + " " +  nama3 + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama3 == 'Junaedi' && peran3 == 'Werewolf') {
  console.log("Selamat datang di Dunia Werewolf, " + nama3 + "\nHalo " +peran3 + " " +  nama3 + ", Kamu akan memakan mangsa setiap malam!")
}



//Output untuk Input nama = 'Jenita' dan peran 'Guard'
//"Selamat datang di Dunia Werewolf, Jenita"
//"Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf."

var nama4 = "Jenita"
var peran4 = "Guard"

if (nama4 == '') {
  console.log('Nama harus diisi!')
} else if (nama4 && peran4 == '') {
  console.log('Halo ' + nama4 + ', Pilih Peranmu untuk memulai game')
} else if (nama4 == 'Jane' && peran4 == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, " + nama4 + "\nHalo " +peran4 + " " +  nama4 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama4 == 'Jenita' && peran4 == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, " + nama4 + "\nHalo " +peran4 + " " +  nama4 + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama4 == 'Junaedi' && peran4 == 'Werewolf') {
  console.log("Selamat datang di Dunia Werewolf, " + nama4 + "\nHalo " +peran4 + " " +  nama4 + ", Kamu akan memakan mangsa setiap malam!")
}



//Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
//"Selamat datang di Dunia Werewolf, Junaedi"
//"Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!" 

var nama5 = "Junaedi"
var peran5 = "Werewolf"

if (nama5 == '') {
  console.log('Nama harus diisi!')
} else if (nama5 && peran5 == '') {
  console.log('Halo ' + nama5 + ', Pilih Peranmu untuk memulai game')
} else if (nama5 == 'Jane' && peran5 == 'Penyihir') {
  console.log("Selamat datang di Dunia Werewolf, " + nama5 + "\nHalo " +peran5 + " " +  nama5 + ", kamu dapat melihat siapa yang menjadi werewolf!")
} else if (nama5 == 'Jenita' && peran5 == 'Guard') {
  console.log("Selamat datang di Dunia Werewolf, " + nama5 + "\nHalo " +peran5 + " " +  nama5 + ", kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else if (nama5 == 'Junaedi' && peran5 == 'Werewolf') {
  console.log("Selamat datang di Dunia Werewolf, " + nama5 + "\nHalo " +peran5 + " " +  nama5 + ", Kamu akan memakan mangsa setiap malam!")
}